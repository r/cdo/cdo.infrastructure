/*
 * Copyright (c) 2004 - 2012 Eike Stepper (Loehne, Germany) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eike Stepper - initial API and implementation
 */
package promoter;

import java.io.File;
import java.util.Comparator;

/**
 * @author Eike Stepper
 */
public abstract class IssueManager extends PromoterComponent implements Comparator<Issue>
{
  private static final boolean USE_CACHE = Boolean.getBoolean("promoter.IssueManager.USE_CACHE");

  private File issuesFolder;

  public IssueManager()
  {
  }

  public synchronized Issue getIssue(String id)
  {
    if (issuesFolder == null)
    {
      issuesFolder = new File(PromoterConfig.INSTANCE.getWorkingArea(), "issues");
      issuesFolder.mkdirs();
    }

    File file = new File(issuesFolder, id);
    if (useCache() && file.isFile())
    {
      return new Issue(file);
    }

    Issue issue = doGetIssue(id);
    if (issue != null)
    {
      issue.write(file);
    }

    return issue;
  }

  public abstract String parseID(String message);

  public abstract String getURL(Issue issue);

  public abstract Integer getSeverity(Issue issue);

  protected abstract Issue doGetIssue(String id);

  protected boolean useCache()
  {
    return USE_CACHE;
  }
}
